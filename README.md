## Text File Validation Service

[![pipeline status](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/text-file-validation/badges/master/pipeline.svg)](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/text-file-validation/-/commits/master)

A service which checks the provided metadata files on the SFTP server. The location is
found based on the system environment (prod,stage,test) and the provided record set id.

[Confluence Doku](https://memobase.atlassian.net/wiki/spaces/TBAS/pages/29196525/Service+Text+File+Validation)

### What does it do?

The service loops through each file on the SFTP server in the record set folder. For each of those
files the extension is checked. If the extension matches a known file type, the file is parsed.

On Fail:
 - Send report with status FATAL and error message. Error message templates 
   can be found in [ReportMessages](src/main/kotlin/ReportMessages.kt)

On Success:
 - Send report with status SUCCESS
 - Send a message to output topic with file type and absolute path to file as content.