/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.exceptions.SftpClientException
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.SftpSettings
import ch.memobase.sftp.SftpClient
import ch.memobase.utility.TextFileValidationMessage
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage
import java.io.File

class KafkaTopology(
    private val inputTopic: String,
    private val outputTopic: String,
    private val reportingTopic: String,
    private val step: String,
    private val appVersion: String,
    sftpSettings: SftpSettings
) {
    private val log = LogManager.getLogger(this::class.java)
    private val sftpClient = SftpClient(sftpSettings)
    private val fileValidation = FileValidation(step, appVersion)

    fun build(): Topology {
        val builder = StreamsBuilder()

        val stream = builder
            .stream<String, String>(inputTopic)
            .mapValues { value -> sftpClient.listFiles(value) }

        stream
            .filter { _, value -> value.isEmpty() }
            .mapValues { readOnlyKey, _ ->
                Report(
                    id = readOnlyKey,
                    status = ReportStatus.fatal,
                    message = "There are no files on the sftp server for this record set.",
                    step = step,
                    stepVersion = appVersion,
                )
            }
            .to(reportingTopic)

        val nonEmptyStream = stream
            .filter { _, value -> value.isNotEmpty() }
            .flatMapValues { value -> value }
            .mapValues { value -> fileValidation.validateExtension(File(value)) }
            .mapValues { value ->
                try {
                    val remoteInputStream = sftpClient.open(value.file)
                    remoteInputStream.use {
                        fileValidation.validate(it, value.format, value.file)
                    }
                } catch (ex: SftpClientException) {
                    log.error(ObjectMessage(ex))
                    ValidationResult(
                        message = TextFileValidationMessage(
                            format = value.format,
                            path = value.file.path,
                        ),
                        report = Report(
                            value.file.name,
                            status = ReportStatus.fatal,
                            message = "Failed to open file on sftp server. Error: ${ex.message}.",
                            step = step,
                            stepVersion = appVersion,
                        )
                    )
                }
            }

        nonEmptyStream
            .mapValues { value ->
                value.report.toJson()
            }
            .to(reportingTopic)

        nonEmptyStream
            .filter { _, value ->
                value.report.status != ReportStatus.fatal
            }
            .mapValues { value ->
                value.message.toJson()
            }
            .to(outputTopic)

        return builder.build()
    }
}