/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.Settings.APP_VERSION_PROPERTY
import ch.memobase.Settings.REPORTING_STEP_NAME_PROPERTY
import ch.memobase.settings.SettingsLoader
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsConfig
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class Service(settings: SettingsLoader) {
    private val log: Logger = LogManager.getLogger(this::class.java)
    private val topology = KafkaTopology(
        settings.inputTopic,
        settings.outputTopic,
        settings.processReportTopic,
        settings.appSettings.getProperty(REPORTING_STEP_NAME_PROPERTY),
        settings.appSettings.getProperty(APP_VERSION_PROPERTY),
        settings.sftpSettings,
    ).build()

    private val hackedSettings = settings.kafkaStreamsSettings
    init {
        hackedSettings.setProperty(StreamsConfig.CONSUMER_PREFIX + ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1")
    }
    private val stream = KafkaStreams(topology, hackedSettings)
    init {
        stream.use {
            try {
                it.start()
                while (it.state().isRunningOrRebalancing) {
                    log.info("Service is running with state: ${stream.state()}")
                    Thread.sleep(300_000L)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            log.error("Service stopped with state: ${stream.state()}")
        }
    }
}
