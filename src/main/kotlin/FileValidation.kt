/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.utility.AcceptedFileFormat
import ch.memobase.utility.TextFileValidationMessage
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.github.doyaaaaaken.kotlincsv.util.MalformedCSVException
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.*
import org.apache.logging.log4j.LogManager
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.xml.sax.SAXException
import java.io.File
import java.io.IOException
import java.io.InputStream
import javax.xml.parsers.DocumentBuilderFactory

class FileValidation(private val step: String, private val stepVersion: String) {
    private val log = LogManager.getLogger(this::class.java)

    private val supportedExtensions = mapOf(
        Pair(Extensions.CSV, AcceptedFileFormat.CSV),
        Pair(Extensions.TSV, AcceptedFileFormat.TSV),
        Pair(Extensions.XLSX, AcceptedFileFormat.XLSX),
        Pair(Extensions.XLS, AcceptedFileFormat.XLS),
        Pair(Extensions.XML, AcceptedFileFormat.XML),
        Pair(Extensions.JSON, AcceptedFileFormat.JSON)
    )

    fun validateExtension(file: File): ExtensionValidationResult {
        return ExtensionValidationResult(
            file,
            supportedExtensions.getOrDefault(file.extension, AcceptedFileFormat.INVALID)
        )
    }

    fun validate(
        inputStream: InputStream,
        format: AcceptedFileFormat,
        file: File
    ): ValidationResult {
        log.info("Validate file with format $format.")
        return when (format) {
            AcceptedFileFormat.CSV, AcceptedFileFormat.TSV -> {
                inputStream.use { stream ->
                    try {
                        csvReader {
                            charset = "UTF-8"
                            delimiter = if (format == AcceptedFileFormat.CSV) ',' else '\t'
                            quoteChar = '"'
                            escapeChar = '\\'
                        }.readAll(stream)
                    } catch (ex: MalformedCSVException) {
                        return@use ValidationResult(
                            TextFileValidationMessage(AcceptedFileFormat.ERROR, file.path),
                            Report(
                                id = file.name,
                                status = ReportStatus.fatal,
                                message = ReportMessages.formatError(file.path, format, ex.localizedMessage),
                                step = step,
                                stepVersion = stepVersion,
                            )
                        )
                    }
                    ValidationResult(
                        TextFileValidationMessage(format, file.path),
                        Report(
                            id = file.name,
                            status = ReportStatus.success,
                            message = ReportMessages.validatedFile(file.path, format),
                            step = step,
                            stepVersion = stepVersion,
                        )
                    )
                }
            }

            AcceptedFileFormat.XLSX, AcceptedFileFormat.XLS -> {
                inputStream.use { stream ->
                    try {
                        WorkbookFactory.create(stream).close()
                    } catch (ex: Exception) {
                        return ValidationResult(
                            TextFileValidationMessage(AcceptedFileFormat.ERROR, file.path),
                            Report(
                                id = file.name,
                                status = ReportStatus.fatal,
                                message = ReportMessages.formatError(file.path, format, ex.localizedMessage),
                                step = step,
                                stepVersion = stepVersion,
                            )
                        )
                    }
                    ValidationResult(
                        TextFileValidationMessage(format, file.path),
                        Report(
                            id = file.name,
                            status = ReportStatus.success,
                            message = ReportMessages.validatedFile(file.path, format),
                            step = step,
                            stepVersion = stepVersion,
                        )
                    )
                }
            }

            AcceptedFileFormat.XML -> {
                inputStream.use {
                    val dbFactory = DocumentBuilderFactory.newInstance()
                    val dBuilder = dbFactory.newDocumentBuilder()
                    try {
                        dBuilder.parse(it)
                        ValidationResult(
                            TextFileValidationMessage(format, file.path),
                            Report(
                                id = file.name,
                                status = ReportStatus.success,
                                message = ReportMessages.validatedFile(file.path, format),
                                step = step,
                                stepVersion = stepVersion,
                            )
                        )
                    } catch (ex: SAXException) {
                        ValidationResult(
                            TextFileValidationMessage(AcceptedFileFormat.ERROR, file.path),
                            Report(
                                id = file.name,
                                status = ReportStatus.fatal,
                                message = ReportMessages.formatError(file.path, format, ex.localizedMessage),
                                step = step,
                                stepVersion = stepVersion,
                            )
                        )
                    } catch (ex: IOException) {
                        ValidationResult(
                            TextFileValidationMessage(AcceptedFileFormat.ERROR, file.path),
                            Report(
                                id = file.name,
                                status = ReportStatus.fatal,
                                message = ReportMessages.formatError(file.path, format, ex.localizedMessage),
                                step = step,
                                stepVersion = stepVersion,
                            )
                        )
                    }
                }
            }

            AcceptedFileFormat.JSON -> {
                validateJSON(inputStream, file)
            }

            else -> ValidationResult(
                TextFileValidationMessage(AcceptedFileFormat.ERROR, file.path),
                Report(
                    id = file.name,
                    status = ReportStatus.fatal,
                    message = ReportMessages.invalidFileExtension(file.name),
                    step = step,
                    stepVersion = stepVersion,
                )
            )
        }
    }

    private fun validateJSON(inputStream: InputStream, file: File): ValidationResult {
        return inputStream.use { iS ->
            try {
                when (val output = Json.parseToJsonElement(iS.readAllBytes().decodeToString())) {
                    is JsonArray -> {
                        ValidationResult(
                            TextFileValidationMessage(AcceptedFileFormat.JSON, file.path),
                            report = Report(
                                id = file.name,
                                status = ReportStatus.fatal,
                                message = ReportMessages.formatError(
                                    file.path,
                                    AcceptedFileFormat.JSON,
                                    "Parsed json structure is an array, expected only a single object"
                                ),
                                step = step,
                                stepVersion = stepVersion,
                            )
                        )
                    }

                    is JsonObject -> {
                        if (output.isEmpty()) {
                            ValidationResult(
                                TextFileValidationMessage(AcceptedFileFormat.JSON, file.path),
                                report = Report(
                                    id = file.name,
                                    status = ReportStatus.fatal,
                                    message = ReportMessages.formatError(
                                        file.path,
                                        AcceptedFileFormat.JSON,
                                        "Parsed json structure is empty"
                                    ),
                                    step = step,
                                    stepVersion = stepVersion,
                                )
                            )
                        } else {
                            ValidationResult(
                                TextFileValidationMessage(AcceptedFileFormat.JSON, file.path),
                                report = Report(
                                    id = file.name,
                                    status = ReportStatus.success,
                                    message = ReportMessages.validatedFile(file.path, AcceptedFileFormat.JSON),
                                    step = step,
                                    stepVersion = stepVersion,
                                )
                            )
                        }
                    }

                    is JsonPrimitive -> {
                        ValidationResult(
                            TextFileValidationMessage(AcceptedFileFormat.JSON, file.path),
                            report = Report(
                                id = file.name,
                                status = ReportStatus.fatal,
                                message = ReportMessages.formatError(
                                    file.path,
                                    AcceptedFileFormat.JSON,
                                    "Not a valid JSON file"
                                ),
                                step = step,
                                stepVersion = stepVersion,
                            )
                        )
                    }

                    JsonNull -> {
                        ValidationResult(
                            TextFileValidationMessage(AcceptedFileFormat.JSON, file.path),
                            report = Report(
                                id = file.name,
                                status = ReportStatus.fatal,
                                message = ReportMessages.formatError(
                                    file.path,
                                    AcceptedFileFormat.JSON,
                                    "Not a valid JSON file"
                                ),
                                step = step,
                                stepVersion = stepVersion,
                            )
                        )
                    }
                }
            } catch (ex: SerializationException) {
                ValidationResult(
                    TextFileValidationMessage(AcceptedFileFormat.JSON, file.path),
                    report = Report(
                        id = file.name,
                        status = ReportStatus.fatal,
                        message = ReportMessages.formatError(
                            file.path,
                            AcceptedFileFormat.JSON,
                            ex.localizedMessage
                        ),
                        step = step,
                        stepVersion = stepVersion,
                    )
                )
            }
        }
    }
}


