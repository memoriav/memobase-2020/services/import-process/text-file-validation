/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.utility.AcceptedFileFormat

object ReportMessages {
    // SUCCESS
    fun validatedFile(path: String, format: AcceptedFileFormat): String {
        return "Validated file at path $path with format $format."
    }

    // FATAL
    fun invalidFileExtension(fileName: String): String {
        return "File Extension Error: Not a valid file extension: $fileName."
    }

    fun formatError(path: String, format: AcceptedFileFormat, message: String): String {
        return "$format ERROR: $message for file $path."
    }
}
