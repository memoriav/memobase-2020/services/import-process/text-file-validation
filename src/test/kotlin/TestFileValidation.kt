/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.test

import ch.memobase.FileValidation
import ch.memobase.ValidationResult
import ch.memobase.reporting.ReportStatus
import ch.memobase.utility.AcceptedFileFormat
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.io.FileInputStream
import java.io.InputStream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestFileValidation {

    private val path = "src/test/resources/data"

    private fun read(format: AcceptedFileFormat, filename: String): InputStream {
        return FileInputStream(File("$path/${format}/$filename"))
    }

    private fun assertValidation(
        result: ValidationResult,
        expectedFormat: AcceptedFileFormat,
        expectedPath: String,
        expectedStatus: String,
        expectedMessage: String

    ) {
        assertAll(
            {
                assertThat(result.message.format)
                    .isEqualTo(expectedFormat)
            },
            {
                assertThat(result.message.path)
                    .isEqualTo(expectedPath)
            },
            {
                assertThat(result.report.status)
                    .isEqualTo(expectedStatus)
            },
            {
                assertThat(result.report.message)
                    .isEqualTo(expectedMessage)
            }
        )
    }

    @Test
    fun `test valid xlsx file`() {
        val validation = FileValidation("test", "testVersion")
        val result = validation.validate(
            read(AcceptedFileFormat.XLSX, "valid.xlsx"),
            AcceptedFileFormat.XLSX,
            File("$path/XLSX/valid.xlsx")
        )
        assertValidation(
            result,
            AcceptedFileFormat.XLSX,
            "src/test/resources/data/XLSX/valid.xlsx",
            ReportStatus.success,
            "Validated file at path src/test/resources/data/XLSX/valid.xlsx with format XLSX."
        )
    }

    @Test
    fun `test valid json file`() {
        val validation = FileValidation("test", "testVersion")
        val result = validation.validate(
            read(AcceptedFileFormat.JSON, "valid.json"),
            AcceptedFileFormat.JSON,
            File("$path/json/valid.json")
        )
        assertValidation(
            result,
            AcceptedFileFormat.JSON,
            "src/test/resources/data/json/valid.json",
            ReportStatus.success,
            "Validated file at path src/test/resources/data/json/valid.json with format JSON."
        )
    }

    @Test
    fun `test empty json file`() {
        val validation = FileValidation("test", "testVersion")
        val result = validation.validate(
            read(AcceptedFileFormat.JSON, "empty.json"),
            AcceptedFileFormat.JSON,
            File("$path/json/empty.json")
        )
        assertValidation(
            result,
            AcceptedFileFormat.JSON,
            "src/test/resources/data/json/empty.json",
            ReportStatus.fatal,
            "JSON ERROR: Parsed json structure is empty for file src/test/resources/data/json/empty.json."
        )
    }

    @Test
    fun `test invalid json file`() {
        val validation = FileValidation("test", "testVersion")
        val result = validation.validate(
            read(AcceptedFileFormat.JSON, "invalid.json"),
            AcceptedFileFormat.JSON,
            File("$path/json/invalid.json")
        )
        assertValidation(
            result,
            AcceptedFileFormat.JSON,
            "src/test/resources/data/json/invalid.json",
            ReportStatus.fatal,
            "JSON ERROR: Unexpected JSON token at offset 4: Expected quotation mark '\"', but had 's' instead at path: \$\n" +
                    "JSON input: {\n" +
                    "  sad21,asd2sad\n" +
                    "} for file src/test/resources/data/json/invalid.json."
        )
    }

    @Test
    fun `test list json file`() {
        val validation = FileValidation("test", "testVersion")
        val result = validation.validate(
            read(AcceptedFileFormat.JSON, "list.json"),
            AcceptedFileFormat.JSON,
            File("$path/json/list.json")
        )
        assertValidation(
            result,
            AcceptedFileFormat.JSON,
            "src/test/resources/data/json/list.json",
            ReportStatus.fatal,
            "JSON ERROR: Parsed json structure is an array, expected only a single object for file src/test/resources/data/json/list.json."
        )
    }
    @Test
    fun `test json with escape character`() {
        val validation = FileValidation("test", "testVersion")
        val result = validation.validate(
            read(AcceptedFileFormat.JSON, "escape_character.json"),
            AcceptedFileFormat.JSON,
            File("$path/json/escape_character.json")
        )
        assertValidation(
            result,
            AcceptedFileFormat.JSON,
            "src/test/resources/data/json/escape_character.json",
            ReportStatus.success,
            "Validated file at path src/test/resources/data/json/escape_character.json with format JSON."
        )
    }
}