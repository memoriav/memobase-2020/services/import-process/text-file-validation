package ch.memobase.test

import ch.memobase.KafkaTopology
import ch.memobase.Settings
import ch.memobase.Settings.loadSettings
import ch.memobase.testing.EmbeddedSftpServer
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import java.io.File
import java.io.FileInputStream

class TestTopology {

    private val resourcePath = "src/test/resources"
    private val sftpServer = EmbeddedSftpServer(22000, "user", "password")
    private val sftpBasePath = "/sftp/sss-001"
    init {
        val directory = File("$resourcePath/data/sftp")
        val files = directory.listFiles()?.filter { it.isFile }
        files?.forEach { file ->
            sftpServer.putFile(
                "$sftpBasePath/${file.name}", FileInputStream("$resourcePath/data/sftp/${file.name}")
            )
            println(file.name)
        }


    }
    private fun headers(): RecordHeaders {
        val headers = RecordHeaders()
        headers.add(RecordHeader("sessionId", "test-session-id".toByteArray()))
        headers.add(RecordHeader("recordSetId", "test-record-set-id".toByteArray()))
        headers.add(RecordHeader("institutionId", "test-institution-id".toByteArray()))
        headers.add(RecordHeader("isPublished", "false".toByteArray()))
        headers.add(RecordHeader("xmlRecordTag", "record".toByteArray()))
        headers.add(RecordHeader("xmlIdentifierFieldName", "identifier".toByteArray()))
        headers.add(RecordHeader("tableSheetIndex", "1".toByteArray()))
        headers.add(RecordHeader("tableHeaderCount", "1".toByteArray()))
        headers.add(RecordHeader("tableHeaderIndex", "1".toByteArray()))
        headers.add(RecordHeader("tableIdentifierIndex", "1".toByteArray()))
        return headers
    }

    @Test
    fun `test sftp pipeline`() {
        val settings = loadSettings("baseSettings.yml")
        val driver = TopologyTestDriver(KafkaTopology(
            settings.inputTopic,
            settings.outputTopic,
            settings.processReportTopic,
            settings.appSettings.getProperty(Settings.REPORTING_STEP_NAME_PROPERTY),
            settings.appSettings.getProperty(Settings.APP_VERSION_PROPERTY),
            settings.sftpSettings

        ).build(), settings.kafkaStreamsSettings)
        driver.use { testDriver ->
            val inputTopic =
                testDriver.createInputTopic(settings.inputTopic, StringSerializer(), StringSerializer())
            val outputTopic =
                testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())
            val reportTopic = testDriver.createOutputTopic(
                settings.processReportTopic, StringDeserializer(), StringDeserializer()
            )

            inputTopic.pipeInput(
                TestRecord(
                    "/sftp/sss-001", "/sftp/sss-001", headers()
                )
            )

            assertAll("sftp pipeline integration", {
                Assertions.assertThat(outputTopic.queueSize).isEqualTo(3)
            }, {
                Assertions.assertThat(reportTopic.queueSize).isEqualTo(9)
            })
        }
    }
}